﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Libreria2013382.Models
{
    public class Libro
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public String Titulo { get; set; }
        [Required]
        public String Publicacion { get; set; }

        [Required]
        public String ImgPortada { get; set; }



        [Required]
        public int IdAutor { get; set; }

        public Autor Autor { get; set; }

        [Required]
        public int IdCategoria { get; set; }

        public Categoria Categoria { get; set; }
    }
}