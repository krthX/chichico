﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Libreria2013382.Models
{
    public class Autor
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public String Nombre { get; set; }

        [Required]
        public String Apellido { get; set; }

        [Required]
        public String Nacimiento { get; set; } 
        
        public String Descripcion { get; set;}

    }
}