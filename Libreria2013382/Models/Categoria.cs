﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Libreria2013382.Models
{
    public class Categoria
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
    }
}