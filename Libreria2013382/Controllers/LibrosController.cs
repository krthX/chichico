﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Libreria2013382.Models;

namespace Libreria2013382.Controllers
{
    public class LibrosController : ApiController
    {
        private Libreria2013382Context db = new Libreria2013382Context();

        // GET api/Libros
        public IQueryable<Libro> GetLibroes()
        {
            return db.Libroes;
        }

        // GET api/Libros/5
        [ResponseType(typeof(Libro))]
        public async Task<IHttpActionResult> GetLibro(int id)
        {
            Libro libro = await db.Libroes.FindAsync(id);
            if (libro == null)
            {
                return NotFound();
            }

            return Ok(libro);
        }

        // PUT api/Libros/5
        public async Task<IHttpActionResult> PutLibro(int id, Libro libro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != libro.Id)
            {
                return BadRequest();
            }

            db.Entry(libro).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LibroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Libros
        [ResponseType(typeof(Libro))]
        public async Task<IHttpActionResult> PostLibro(Libro libro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Libroes.Add(libro);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = libro.Id }, libro);
        }

        // DELETE api/Libros/5
        [ResponseType(typeof(Libro))]
        public async Task<IHttpActionResult> DeleteLibro(int id)
        {
            Libro libro = await db.Libroes.FindAsync(id);
            if (libro == null)
            {
                return NotFound();
            }

            db.Libroes.Remove(libro);
            await db.SaveChangesAsync();

            return Ok(libro);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LibroExists(int id)
        {
            return db.Libroes.Count(e => e.Id == id) > 0;
        }
    }
}