﻿using System.Web;
using System.Web.Mvc;

namespace Libreria2013382
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
